import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeatMapGoogleComponent } from './component/heat-map-google/heat-map-google.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {LoadingModule} from 'ngx-loading';
import {AgmCoreModule} from '@agm/core';
import {GoogleApiModule, NG_GAPI_CONFIG, NgGapiClientConfig} from 'ng-gapi';

const gapiClientConfig: NgGapiClientConfig = {
  client_id: '593624877948-q65ok8pnb23fcal3k60847g267mk7dr2.apps.googleusercontent.com',
  discoveryDocs: ['https://analyticsreporting.googleapis.com/$discovery/rest?version=v4'],
  scope: [
    'https://www.googleapis.com/auth/analytics.readonly',
    'https://www.googleapis.com/auth/analytics',
    'https://www.googleapis.com/fusiontables/v1/tables'
  ].join(' ')
};

@NgModule({
  declarations: [
    AppComponent,
    HeatMapGoogleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    LoadingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBcIfEqaaHMwc9_a-Cwy7eI40QT0pXrjJU'
    }),
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
