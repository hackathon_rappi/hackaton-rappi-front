export class Toolkit {
  constructor(
    public trusted?: boolean,
    public vehicle?: number,
    public kit_size?: number,
    public know_how?: number,
    public terminal?: number,
    public exclusive?: boolean,
    public order_level?: number,
    public delivery_kit?: number,
    public storekeeper_level?: number
  ) {}
}
