import {Toolkit} from './toolkit';

export class Order {
  constructor(
    public id?: string,
    public lat?: number,
    public lng?: number,
    public timestamp?: Date,
    public created_at?: Date,
    public type?: string,
    public toolkit?: Toolkit
  ) {}
}
