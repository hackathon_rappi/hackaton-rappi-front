export class SaturationItem {
  constructor(
    public lat: number,
    public lng: number,
    public weight: number
  ) {}
}
