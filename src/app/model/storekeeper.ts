import {Toolkit} from './toolkit';

export class Storekeeper {
  constructor(
    public id?: string,
    public storekeeper_id?: string,
    public lat?: number,
    public lng?: number,
    public date: Date = new Date('2018-11-08T00:00:00'),
    public time: Date = new Date('2018-11-08T12:00:00'),
    public timestamp?: Date,
    public toolkit?: Toolkit
  ) {}

  toLatLng() {
    return new google.maps.LatLng(37.782551, -122.445368);
  }
}

