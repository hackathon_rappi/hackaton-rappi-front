import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatMapGoogleComponent } from './heat-map-google.component';

describe('HeatMapGoogleComponent', () => {
  let component: HeatMapGoogleComponent;
  let fixture: ComponentFixture<HeatMapGoogleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeatMapGoogleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatMapGoogleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
