import {Component, OnInit, ViewChild} from '@angular/core';
import { } from '@types/googlemaps';
import {StorekeeperService} from './service/storekeeper.service';
import {Storekeeper} from './model/storekeeper';
import LatLng = google.maps.LatLng;
import Map = google.maps.Map;
import HeatmapLayer = google.maps.visualization.HeatmapLayer;
import {OrderService} from './service/order.service';
import {Order} from './model/order';
import {StorekeeperFilter} from './filter/storekeeper-filter';
import {OrderFilter} from './filter/order-filter';
import {DatePipe} from '@angular/common';
import {SaturationItem} from './model/saturation-item';
import {SaturationService} from './service/saturation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [StorekeeperService, OrderService, SaturationService]
})
export class AppComponent implements OnInit {
  private storekeepers: Storekeeper[] = [];
  private orders: Order[] = [];
  private points: LatLng[] = [];
  private saturationItems: SaturationItem[] = [];
  private map;
  private heatmap;

  storekeeperDate;
  storekeeperTime;

  orderDate;
  orderTime;

  saturationDate;
  saturationTime;
  saturationTimestamp;

  toggleButton = false;
  saturationOn = true;

  storekeeperId = 0;

  buttonName = 'Órdenes';

  public storekeeperFilter = new StorekeeperFilter();
  public orderFilter = new OrderFilter();

  constructor(private storekeeperService: StorekeeperService,
              private orderService: OrderService,
              private saturationService: SaturationService) {
  }

  ngOnInit() {
    this.initMap();
    this.storekeeperDate = new DatePipe('en-US').transform(this.storekeeperFilter.date, 'yyyy-MM-dd');
    this.storekeeperTime = new DatePipe('en-US').transform(this.storekeeperFilter.time, 'HH:mm:00');
    this.orderDate = new DatePipe('en-US').transform(this.orderFilter.date, 'yyyy-MM-dd');
    this.orderTime = new DatePipe('en-US').transform(this.orderFilter.time, 'HH:mm:00');
    this.saturationDate = new DatePipe('en-US').transform(this.orderFilter.date, 'yyyy-MM-dd');
    this.saturationTime = new DatePipe('en-US').transform(this.orderFilter.time, 'HH:mm:00');

    this.update();
    this.saturation();
  }

  initMap() {
    this.map = new Map(document.getElementById('map'), {
      zoom: 12,
      center: {lat: 4.624335, lng: -74.063644}
    });
  }

  resetMap() {
    this.map = new Map(document.getElementById('map'), {
      zoom: 12,
      center: {lat: 4.624335, lng: -74.063644}
    });
  }

  public toggle() {
    this.saturationOn = false;
    this.buttonName = (this.toggleButton) ? 'Órdenes' : 'Rappitenderos';
    this.toggleButton = !this.toggleButton;
  }

  public saturation() {
    this.saturationOn = true;
    this.saturationService
      .index(this.saturationTimestamp)
      .subscribe(saturationItems => {
        this.saturationItems = saturationItems;
        const heatMapData = [];
        for (const saturationItem of saturationItems) {
          heatMapData.push({location: new google.maps.LatLng(saturationItem.lat, saturationItem.lng), weight: saturationItem.weight});
        }
        this.resetMap();
        this.heatmap = new google.maps.visualization.HeatmapLayer({
          data: heatMapData
        });
        this.heatmap.setMap(this.map);
        this.heatmap.set('radius', this.heatmap.get('radius') ? null : 100);
      });
  }

  recomendation() {
    this.storekeeperService
      .get(this.storekeeperId)
      .subscribe(data => {
        console.log(JSON.stringify(data));
        this.resetMap();
        const marker = new google.maps.Marker({
          position: new google.maps.LatLng(0, 0),
          map: this.map,
          title: 'Recomendación'
        });
      });
  }

  setHeatmap() {
    this.heatmap = new HeatmapLayer({
      data: this.points,
      map: this.map
    });
    const gradient = [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 255, 1)',
      'rgba(0, 191, 255, 1)',
      'rgba(0, 127, 255, 1)',
      'rgba(0, 63, 255, 1)',
      'rgba(0, 0, 255, 1)',
      'rgba(0, 0, 223, 1)',
      'rgba(0, 0, 191, 1)',
      'rgba(0, 0, 159, 1)',
      'rgba(0, 0, 127, 1)',
      'rgba(63, 0, 91, 1)',
      'rgba(127, 0, 63, 1)',
      'rgba(191, 0, 31, 1)',
      'rgba(255, 0, 0, 1)'
    ];
    this.heatmap.set('gradient', this.heatmap.get('gradient') ? null : gradient);
  }

  getStorekeepersDiagnostic() {
    return JSON.stringify(this.storekeeperFilter);
  }

  getOrdersDiagnostic() {
    return JSON.stringify(this.orderFilter);
  }

  filterByStorekeeper() {
    this.storekeeperService
      .index(this.storekeeperFilter)
      .subscribe(storekeepers => {
        this.storekeepers = storekeepers;
        for (const storekeeper of storekeepers) {
          this.points.push(new LatLng(storekeeper.lat, storekeeper.lng));
        }
        this.resetMap();
        this.setHeatmap();
        console.log(this.storekeepers.length);
      });
  }

  filterByOrder() {
    this.orderService
      .index(this.orderFilter)
      .subscribe(orders => {
        this.orders = orders;
        for (const order of orders) {
          this.points.push(new LatLng(order.lat, order.lng));
        }
        this.resetMap();
        this.setHeatmap();
        console.log(this.orders.length);
      });
  }

  update() {
    this.storekeeperFilter.date = this.storekeeperDate;
    this.storekeeperFilter.time = this.storekeeperTime;
    this.orderFilter.date = this.orderDate;
    this.orderFilter.time = this.orderTime;
    this.saturationTimestamp = this.saturationDate + ' ' + this.saturationTime;
  }
}
