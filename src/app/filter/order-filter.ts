import {DatePipe, Time} from '@angular/common';

export class OrderFilter {
  constructor(
    public geohash?: string,
    public bike: boolean = true,
    public motorcycle: boolean = true,
    public vehicles: number[] = [],
    public restaurant: boolean = true,
    public mercado: boolean = true,
    public ultraservicio: boolean = false,
    public courier: boolean = false,
    public whim: boolean = false,
    public express: boolean = false,
    public expressTablet: boolean = false,
    public type: string[] = [],
    public date: Date = new Date('2018-09-06'),
    public time: Date = new Date('2018-09-06 12:00:00'),
    public timestamp: string = ''
  ) {
    this.timestamp = new DatePipe('en-US').transform(this.date, 'yyyy-MM-dd')
      + ' ' + new DatePipe('en-US').transform(this.time, 'HH:mm:ss');
  }

  getParams() {
    this.timestamp = this.date + ' ' + this.time;
    const params = {
      timestamp: this.timestamp,
    };
    if (this.geohash && this.geohash !== '') {
      params['geohash'] = this.geohash;
    }
    this.vehicles = [];
    if (this.bike) {
      this.vehicles.push(1);
    } if (this.motorcycle) {
      this.vehicles.push(2);
    } if (!this.bike && !this.motorcycle) {
      this.vehicles = undefined;
    }
    if (this.vehicles) {
      params['vehicle'] = '[' + this.vehicles + ']';
    }
    this.type = [];
    if (this.restaurant) {
      this.type.push('\"restaurant\"');
    } if (this.mercado) {
      this.type.push('\"market\"');
    } if (this.ultraservicio) {
      this.type.push('\"ultraservicio\"');
    } if (this.courier) {
      this.type.push('\"courier\"');
    } if (this.whim) {
      this.type.push('\"whim\"');
    } if (this.express) {
      this.type.push('\"express\"');
    } if (this.expressTablet) {
      this.type.push('\"express_tablet\"');
    } if (!this.restaurant && !this.mercado && !this.ultraservicio && !this.courier && !this.whim && !this.express && !this.expressTablet) {
      this.type = undefined;
    }
    if (this.type) {
      params['type'] = '[' + this.type + ']';
    }

    return params;
  }
}
