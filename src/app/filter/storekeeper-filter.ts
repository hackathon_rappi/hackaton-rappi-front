import {DatePipe, Time} from '@angular/common';

export class StorekeeperFilter {
  constructor(
    public geohash?: string,
    public bike: boolean = true,
    public motorcycle: boolean = true,
    public vehicles: number[] = [],
    public date: Date = new Date('2018-09-11'),
    public time: Date = new Date('2018-09-11Z12:00:00'),
    public timestamp: string = ''
  ) {
    this.timestamp = new DatePipe('en-US').transform(this.date, 'yyyy-MM-dd')
      + ' ' + new DatePipe('en-US').transform(this.time, 'HH:mm:ss');
  }

  getParams() {
    this.timestamp = this.date + ' ' + this.time;
    const params = {
      timestamp: '\'' + this.timestamp + '\'',
    };
    if (this.geohash && this.geohash !== '') {
      params['geohash'] = this.geohash;
    }
    this.vehicles = [];
    if (this.bike) {
      this.vehicles.push(1);
    } if (this.motorcycle) {
      this.vehicles.push(2);
    } if (!this.bike && !this.motorcycle) {
      this.vehicles = undefined;
    }
    console.log(this.vehicles);
    if (this.vehicles) {
      params['vehicle'] = '[' + this.vehicles + ']';
    }

    return params;
  }
}
