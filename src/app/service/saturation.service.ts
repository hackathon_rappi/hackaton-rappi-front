import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SaturationItem} from '../model/saturation-item';

@Injectable({
  providedIn: 'root'
})
export class SaturationService {

  private saturationUrl = 'http://hackaton-rappi.eastus.cloudapp.azure.com:5000/saturation';

  constructor(private http: HttpClient) { }

  private getHeaders() {
    return new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
  }

  index(timestamp: string): Observable<SaturationItem[]> {
    return this.http
      .get<SaturationItem[]>(this.saturationUrl,
        {
          params: {timestamp: timestamp},
          headers: this.getHeaders()
        });
  }
}
