import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Order} from '../model/order';
import {Observable} from 'rxjs';
import {OrderFilter} from '../filter/order-filter';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private ordersUrl = 'http://hackaton-rappi.eastus.cloudapp.azure.com:5000/orders';

  constructor(private http: HttpClient) { }

  private getHeaders() {
    return new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
  }

  index(orderFilter: OrderFilter): Observable<Order[]> {
    return this.http
      .get<Order[]>(
        this.ordersUrl,
        {
          params: orderFilter.getParams(),
          headers: this.getHeaders()
        }
      );
  }
}
