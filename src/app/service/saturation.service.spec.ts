import { TestBed, inject } from '@angular/core/testing';

import { SaturationService } from './saturation.service';

describe('SaturationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaturationService]
    });
  });

  it('should be created', inject([SaturationService], (service: SaturationService) => {
    expect(service).toBeTruthy();
  }));
});
