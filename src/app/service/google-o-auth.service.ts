import { Injectable } from '@angular/core';
import {GoogleAuthService} from 'ng-gapi';
import GoogleUser = gapi.auth2.GoogleUser;

@Injectable({
  providedIn: 'root'
})
export class GoogleOAuthService {

  public static SESSION_STORAGE_KEY = 'accessToken';
  public static OAUTH_TOKEN = '';
  private user: GoogleUser;

  constructor(private googleAuth: GoogleAuthService) {
  }

  public getToken(): string {
    const token: string = sessionStorage.getItem(GoogleOAuthService.SESSION_STORAGE_KEY);
    if (!token) {
      throw new Error('no token set , authentication required');
    }
    GoogleOAuthService.OAUTH_TOKEN = token;
    return sessionStorage.getItem(GoogleOAuthService.SESSION_STORAGE_KEY);
  }

  public signIn(): void {
    this.googleAuth.getAuth()
      .subscribe((auth) => {
        auth.signIn().then(res => this.signInSuccessHandler(res));
      });
  }

  private signInSuccessHandler(res: GoogleUser) {
    this.user = res;
    sessionStorage.setItem(
      GoogleOAuthService.SESSION_STORAGE_KEY, res.getAuthResponse().access_token
    );
  }
}
