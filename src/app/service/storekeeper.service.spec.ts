import { TestBed, inject } from '@angular/core/testing';

import { StorekeeperService } from './storekeeper.service';

describe('StorekeeperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StorekeeperService]
    });
  });

  it('should be created', inject([StorekeeperService], (service: StorekeeperService) => {
    expect(service).toBeTruthy();
  }));
});
