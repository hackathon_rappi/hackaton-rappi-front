import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Storekeeper} from '../model/storekeeper';
import {StorekeeperFilter} from '../filter/storekeeper-filter';
import {convertToParamMap} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StorekeeperService {

  private storekeepersUrl = 'http://hackaton-rappi.eastus.cloudapp.azure.com:5000/storekeepers';

  constructor(private http: HttpClient) { }

  private getHeaders() {
    return new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
  }

  index(storekeeperFilter: StorekeeperFilter): Observable<Storekeeper[]> {
    return this.http
      .get<Storekeeper[]>(
        this.storekeepersUrl,
        {
          params: storekeeperFilter.getParams(),
          headers: this.getHeaders()
        }
      );
  }

  get(storekeeperId): Observable<any> {
    return this.http
      .get<any>(
        this.storekeepersUrl,
        {
          params: { storekeeper_id: storekeeperId },
          headers: this.getHeaders()
        }
      );
  }
}
